class Node:
    def __init__(self,data):
        self.data = data
        self.next = None
        self.prev = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None

    def append(self,data):
        new_node = Node(data)
        if self.head is None:
            self.head = new_node
            return

        cur_node = self.head

        while cur_node:
            if cur_node.next is None:
              cur_node.next = new_node
              new_node.prev = cur_node
              new_node.next = None
              return 
            cur_node = cur_node.next 
            cur_node.prev
        
        
    
    def prepend(self,data):
        new_node = Node(data)
        if self.head is None:
            self.head = new_node
            return
        

        new_node.next = self.head
        self.head.prev = new_node
        self.head = new_node
        new_node.prev = None
        

    
    def print_list(self):
         cur_node = self.head

         while(cur_node):
             print(cur_node.data)
             cur_node = cur_node.next

    
dlist = DoublyLinkedList()

dlist.append(3)
dlist.append(4)

dlist.print_list()
print("After prepend")
dlist.prepend(2)
dlist.prepend(1)
dlist.print_list()
print(dlist.head.next.next.prev.data)