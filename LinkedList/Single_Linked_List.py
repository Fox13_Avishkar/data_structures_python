class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None
#Printing the list
    def print_list(self):
        cur_node = self.head
        while cur_node:
            print(cur_node.data)
            cur_node = cur_node.next

# Adding node at the head of the list
    def prepend(self, data):
        new_node = Node(data)

        if self.head is None:
          self.head = new_node
          return
        
        new_node.next = self.head
        self.head = new_node

#Adding node after a certain node
    def insert_after_node(self,prev_node,data):

        new_node = Node(data)
        cur_node = self.head

        while(cur_node.data != prev_node):
            cur_node = cur_node.next
            if(cur_node is None):
                print("previous node is not present in the list")
                return

        new_node.next = cur_node.next
        cur_node.next = new_node


#Adding node at the end of the list
    def append(self,data):

        new_node = Node(data)

        if self.head is None: # check if the head is none or not.if none then it is an emppty lisnkedlist
            self.head = new_node
            return

        last_node = self.head

        while last_node.next:
            last_node = last_node.next
        last_node.next = new_node

#Deleting a Node
    def delete_node(self,key):
        cur_node = self.head
        if  cur_node and cur_node.data == key:
            self.head = cur_node.next
            cur_node = None

        #keeping track of the second record
        prev_node = cur_node
        #starting from the second node
        cur_node = cur_node.next

        while cur_node and cur_node.data!=key :
            prev_node = cur_node
            cur_node = cur_node.next

        if cur_node is None:
            print("No Node found with the value")
            return

        prev_node.next = cur_node.next
        cur_node = None

        """ # Another logic

            #if the cur_node.node.next exsist and cur_node.data is equal to the key
            if cur_node.next and cur_node.next.data == key:
                #cur_node.next
                cur_node.next = None
                print("del sucessful")
                return
            prev_node = cur_node
            cur_node = cur_node.next

        print("The element does not exist in the list")"""

    def delete_node_at_position(self,pos):
        index = 0
        cur_node = self.head

        if cur_node and index == pos:
            self.head = cur_node.next
            cur_node = None
            return
        index +=1
        prev_node = cur_node
        cur_node = cur_node.next
        while cur_node:
            if(index == pos):
                prev_node.next = cur_node.next
                cur_node = None
                print("Deletion sucessful")
                return
            prev_node = cur_node
            cur_node = cur_node.next
            index +=1

        print("No Position on the index")

    def len_iter(self):
        cur_node = self.head
        count = 0
        if cur_node is None:
            Print("The list is empty")
            return

        while cur_node:
            count = count + 1
            cur_node = cur_node.next

        print("The length of the list is",count)

    



llist = LinkedList()
llist.append("A")
llist.append("B")

llist.append("D")
llist.insert_after_node("B","C")
llist.delete_node("D")
llist.print_list()
llist.delete_node_at_position(2)
llist.print_list()
llist.len_iter()
