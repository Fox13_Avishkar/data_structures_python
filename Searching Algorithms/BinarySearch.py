def BinarySearch(data,low,high,target): 
    if low <= high:
    
        mid = (low + high)//2
        if target == data[mid]:
            return True
        elif target < data[mid]:
            return BinarySearch(data,low,mid-1,target)
        else :
            return BinarySearch(data,mid+1,high,target)

    else:
        return False


    


if __name__ == "__main__":
    data = ([int(x) for x in input("Enter the numbers using a single space").split()])
    data.sort()
    target = int(input("enter the number to be found using Binary Search"))
    if (BinarySearch(data,0,len(data)-1,target)):
        print("The element is present in the list")
    else:
        print("No elements found")